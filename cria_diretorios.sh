#!/bin/bash

##############################################################
#                                                            #
# Cria a estrutura de diretorios para persistencia dos dados #
#                                                            #
##############################################################

echo "Criando a estrutura de diretorios!"
echo
echo -n "Aguarde"
for i in $(seq 1 1 5);
do
        echo -n "."
        sleep 01
        echo -ne ""
done
echo
mkdir -p data/var/lib/mysql
mkdir -p data/etc/rundeck
mkdir -p data/var/rundeck
mkdir -p data/var/lib/rundeck/.ssh
mkdir -p data/var/log/rundeck
mkdir -p data/opt/rundeck-plugins
mkdir -p data/var/lib/rundeck/logs
mkdir -p data/var/lib/rundeck/var/storag
echo "************"
echo "Finalizado!"
echo
ls -ltr $(pwd)/data
