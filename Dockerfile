ARG RUNDECK_IMAGE

FROM ${RUNDECK_IMAGE:-jordan/rundeck:latest}

ENV TZ=America/Sao_Paulo

COPY ./requirements.txt / 

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
    iputils-ping \
    curl \
    htop \
    lxc \
    net-tools \
    git-core \
    sshpass \
    libaio1 \
    unzip \
    python3-pip python3-dev \
    vim \
    sudo 

RUN echo 'Defaults:       rundeck !authenticate' >> /etc/sudoers
RUN echo 'rundeck ALL=NOPASSWD: /usr/bin/docker' >> /etc/sudoers
RUN pip3 install --upgrade pip
RUN pip3 install -r /requirements.txt
    
RUN cd /usr/local/bin && ln -s /usr/bin/python3 python
	
RUN rm -rf /var/lib/apt/lists/


##########
# DOCKER #
##########
#RUN groupadd docker
#RUN usermod -aG docker rundeck
#RUN newgrp docker
#RUN curl -sSL https://get.docker.com/ | sh
##RUN curl https://releases.rancher.com/install-docker/18.09.sh | sh
## Wrapper docker ninja...
#COPY ./wrapdocker /usr/local/bin/wrapdocker
#RUN chmod +x /usr/local/bin/wrapdocker
#RUN usermod -aG docker rundeck
#VOLUME /var/lib/docker
#CMD ["wrapdocker"]

##################################################
# PIP - docker compose, pywinrm, boto3 e ansible #
##################################################
#RUN pip3 install docker-compose pywinrm boto3 pyvmomi ansible six==1.11.0 cryptography==2.5
#RUN docker-compose version



###########
# KUBECTL #
###########
#RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
#    && chmod +x ./kubectl \
#    && mv ./kubectl /usr/local/bin/kubectl


#-------------------#
RUN chmod -R 777 /var/lib/mysql
RUN chmod -R 777 /etc/rundeck
RUN chmod -R 777 /var/lib/rundeck/.ssh
RUN chmod -R 777 /var/log/rundeck
RUN chmod -R 777 /var/lib/rundeck/logs

##########################
# ALTERANDO O ENTRYPOINT #
##########################
COPY ./configura.sh /usr/local/bin/
COPY ./chown.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/configura.sh
RUN chmod +x /usr/local/bin/chown.sh
#CMD [/usr/local/bin/configura.sh"]
